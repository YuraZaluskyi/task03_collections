package com.zaluskyi.priorityqueue;

import java.util.Queue;

public class Main {
    public static void main(String[] args) {
        PriorityQueue<String> priorityQueue = new PriorityQueue(10);
        priorityQueue.insert("10");
        priorityQueue.insert("5");
        priorityQueue.insert("4");
        priorityQueue.insert("23");

        System.out.println(priorityQueue.remove());
    }
}
