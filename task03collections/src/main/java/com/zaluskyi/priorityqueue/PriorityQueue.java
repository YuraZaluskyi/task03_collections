package com.zaluskyi.priorityqueue;

import java.util.Arrays;

public class PriorityQueue<T extends Comparable<T>> {
    private int maxSize;
    private Object[] queArray;
    private int nItems;


    public PriorityQueue(int size) {
        maxSize = size;
        queArray = new Object[size];
        nItems = 0;
    }

    public void insert(T item) {
        int j = 0;
        if (nItems == 0) {
            queArray[nItems++] = item;
        } else {
            for (j = nItems - 1; j >= 0; j--) {
                if (item.compareTo((T)queArray[j]) == 1) {
                    queArray[j + 1] = queArray[j];
                } else {
                    break;
                }
            }
        }
        queArray[j + 1] = item;
        nItems++;
    }

    public T remove() {
        return (T) queArray[--nItems];
    }

    public T peekMin() {
        return (T) queArray[nItems - 1];
    }

    public boolean isEmpty() {
        return (nItems == 0);
    }

    public boolean isFull() {
        return (nItems == maxSize);
    }

    @Override
    public String toString() {
        return "PriorityQueue{" +
                "maxSize=" + maxSize +
                ", queArray=" + Arrays.toString(queArray) +
                ", nItems=" + nItems +
                '}';
    }
}
